var app = require('express');
var request = require('request');
var cheerio = require('cheerio');
var router = app.Router();

router.get('/', function (req, res, next) {
  res.json({property: 'test'});
});

router.post('/', function (req, res, next) {
  var peDom, asDom;
  var peArticleUrl = buildArticleUrl('http://demorgensite.test.persgroep.be/', req.body.pe),
      asArticleUrl = buildArticleUrl('http://demorgensite2.test.persgroep.be/', req.body.as);

  request(peArticleUrl, function (error, response, body) {
    peDom = cheerio.load(body);
    compare();
  });

  request(asArticleUrl, function (error, response, body) {
    asDom = cheerio.load(body);
    compare();
  });

  function compare() {
    if (peDom && asDom) {
      var sanitizedPeArticle = extractAndSanitizeArticle(peDom);
      var sanitizedAsArticle = extractAndSanitizeArticle(asDom);
      var result = sanitizedPeArticle === sanitizedAsArticle;
      res.json({
        result: result,
        pe: sanitizedPeArticle,
        as: sanitizedAsArticle
      });
    }
  }

  function extractAndSanitizeArticle(dom) {
    var article = dom('.article');

    var footer = article.find('.article__footer');
    var body = article.find('.article__body');
    var paywall = article.find('.paywall');

    footer.remove();
    body.remove();
    paywall.remove();

    return article.html();
  }

  function buildArticleUrl(baseUrl, id) {
    console.log(baseUrl + id);
    return baseUrl + id;
  }
});

module.exports = router;
